#include <stdlib.h>
#include <cstdio>

#include "Container.h"
#include "Config.h"
#include "Logger.h"

using namespace doubrtom;

Container Container::instance;

Container & Container::getInstance() {
    return instance;
}

Container::Container() {
    config = NULL;
}

void Container::clear() {
    if (config != NULL) delete config;
    if (logger != NULL) delete logger;
}

Config & Container::getConfig() {
    if (config == NULL) {
        this->config = new Config();
    }
    
    return *config;
}

Logger & Container::getLogger() {
    if (logger == NULL) createLogger();
    
    return *logger;
}

void Container::createLogger() {
    Config & config = getConfig();
    logger = new Logger(
        config.getLogFile(),
        (config.getLogType() == Config::FILE) ? false : true
    );
}
