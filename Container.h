/* 
 * File:   Container.h
 * Author: tomas
 *
 * Created on 5. únor 2014, 20:00
 */

#ifndef CONTAINER_H_64789024876378
#define	CONTAINER_H_64789024876378

namespace doubrtom {
    
    class Config;
    class Logger;
    
    class Container {
    public:
        void clear();
        static Container & getInstance();
        
        Config & getConfig();
        Logger & getLogger();
        void createLogger();
        
    protected:
        Container();
        
        static Container instance;
        Config * config;
        Logger * logger;
    };
    
}


#endif	/* CONTAINER_H */

