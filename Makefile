SOURCE=Logger.h Logger.cpp Writer.h Writer.cpp StdoutWriter.h StdoutWriter.cpp
CXX=g++
CXXFLAGS= -O2 -Wall

#---------------

all: vncproxy
	
vncproxy: Config.o Container.o Exception.o Logger.o
	$(CXX) $(CXXFLAGS) Config.o Container.o Exception.o main.cpp Logger.o -o a.out -fopenmp
	
Config.o: Config.cpp Config.h
	$(CXX) -c $(CXXFLAGS) Config.cpp Config.h
	
Container.o: Container.cpp Container.h
	$(CXX) -c $(CXXFLAGS) Container.cpp Container.h
	
Exception.o: Exception.h Exception.cpp
	$(CXX) -c $(CXXFLAGS) Exception.h Exception.cpp
	
Logger.o: Logger.h Logger.cpp
	$(CXX) -c $(CXXFLAGS) Logger.h Logger.cpp

clean:
	rm -f *.o