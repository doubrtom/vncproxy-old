#include <cstdio>
#include <cstdarg>
#include <iostream>
#include <fstream>

#include "Logger.h"
#include "Exception.h"

using namespace doubrtom;
using namespace std;

Logger::Logger(const char * filePath, bool useStdout) {
    if (filePath != 0) {
        useFile = true;
        file.open(filePath);
        if (!file.is_open()) {
            cerr << "Unable to open log file: %s. Disable file log." << endl;
            useFile = false;
        }
    } else useFile = false;

    this->useStdout = useStdout;
}

Logger::~Logger() {
    if (file.is_open()) file.close();
}

void Logger::error(const char * str, ...) {
    va_list args;
    va_start(args, str);
    vsprintf(buffer, str, args);
    va_end(args);

    write(ERROR);
}

void Logger::info(const char * str, ...) {
    va_list args;
    va_start(args, str);
    vsprintf(buffer, str, args);
    va_end(args);

    write(INFO);
}

void Logger::write(LogType type) {
    if (useFile) file << getDateTime() << ": " << buffer << endl;
    if (useStdout && type == INFO) cout << buffer << endl;
    if (type == ERROR) cerr << buffer << endl;
}

// code from stackoverflow
// http://stackoverflow.com/questions/997946/how-to-get-current-time-and-date-in-c
const std::string Logger::getDateTime() {
    time_t now = time(0);
    struct tm tstruct;
    char buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof (buf), "%Y-%m-%d %X", &tstruct);

    return buf;
}