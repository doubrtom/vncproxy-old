#include <cstdlib>
#include <string.h>
#include <stdio.h>

#include "Exception.h"

using namespace doubrtom;

Exception::Exception() {
    msg[0] = 0;
}

Exception::Exception(const char* msg) {
    memcpy(this->msg, msg, sizeof(char)*strlen(msg) + 1);
}

Exception::Exception(const char * msg, int arg) {
    sprintf(this->msg, msg, arg);
}

Exception::Exception(const char * msg, const char * arg) {
    sprintf(this->msg, msg, arg);
}

const char * Exception::getMessage() const {
    return msg;
}
