#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <iostream>
#include <fstream>

#include "Exception.h"
#include "Config.h"

using namespace std;
using namespace doubrtom;

Config::Config() {
    homeDir = getenv("HOME");
    if (homeDir == NULL) throw doubrtom::Exception("Missing environment variable HOME.");
    
    sprintf(programDir, "%s/.vncproxy", homeDir);
    proxyDisplayNum = 0;
    vncServerDisplayNum = 0;
    serverMode = TRANSPARENT;
    clientMode = TRANSPARENT;
    logType = ALL;
    imageDir[0] = 0;
    recordFile[0] = 0;
    sprintf(logFile, "%s/log.log", programDir);
    sprintf(host, "localhost");
}

/* ----------------- methods ---------------------*/

void Config::processParameters(int argc, char** argv) {
    for (int i=2; i < argc; i+=2) {
        if (argv[i][0] == '-' && i+1 < argc) {
            setValue(&(argv[i][1]), argv[i+1]);
        }
    }
    
    readDisplayNumber(argv[1]);
}

void Config::readDisplayNumber(const char * str) {
    const char * errorMsg = "Display number has to be first parameter in format :display";
    
    if (str[0] != ':') throw Exception(errorMsg);
    
    int display = atoi(&(str[1]));
    if (display == 0) throw Exception(errorMsg);
    
    proxyDisplayNum = display;
}

void Config::setValue(const char * key, const char * value) {
    if (!strcmp(key, "server")) setVncServer(value);
    else if (!strcmp(key, "clientmode")) setClientMode(value);
    else if (!strcmp(key, "servermode")) setServerMode(value);
    else if (!strcmp(key, "log")) setLogType(value);
    else if (!strcmp(key, "logfile")) setLogFile(value);
    else if (!strcmp(key, "imagedir")) setImageDir(value);
    else if (!strcmp(key, "recordfile")) setRecordFile(value);
    else throw Exception("Unknown vncproxy parametr: %s", key);
}

void Config::createProgramDir() {
    int result = mkdir(programDir, 0755);
    if (result == -1 && errno != EEXIST) {
        throw Exception("Unable to create vncproxy directory ~/.vncproxy");
    }
}

void Config::readConfigFile() {
    string line, key, value;
    char configFilePath[strlen(programDir) + 12];
    sprintf(configFilePath, "%s/config.txt", programDir);
    
    ifstream configFile(configFilePath, ios::in);
    if (!configFile.is_open()) return; // if no config file, do nothing
    
    if (!getline(configFile, line)) {
        configFile.close();
        return;
    }
    if (line.compare("vncproxy")) {
        throw Exception("Config file is corrupted, please repair it or remove it. Path is ~/.vncproxy/config.txt");
    }
    
    while (getline(configFile, key, '=') && getline(configFile, value)) {
        setValue(key.c_str(), value.c_str());
    }
    
    configFile.close();
}

void Config::checkConfig() {
    if (vncServerDisplayNum == 0) vncServerDisplayNum = proxyDisplayNum - 1;
    if (clientMode == SAVE && recordFile[0] == 0) {
        throw Exception("Unspecified file for recording.");
    }
    if (serverMode == SAVE && imageDir[0] == 0) {
        throw Exception("Unscpecified directory for images.");
    }
}

/* --------------- setters & getters ------------ */

int Config::getProxyDisplay() const {
    return proxyDisplayNum;
}

int Config::getVncServerDisplay() const {
    return vncServerDisplayNum;
}

Config::Mode Config::getServerMode() const {
    return serverMode;
}

Config::Mode Config::getClientMode() const {
    return clientMode;
}

Config::LogType Config::getLogType() const {
    return logType;
}

const char * Config::getImageDir() const {
    return imageDir;
}

const char * Config::getRecordFile() const {
    return recordFile;
}

const char * Config::getLogFile() const {
    return logFile;
}

void Config::setProxyDisplay(int port) {
    proxyDisplayNum = port;
}

void Config::setVncServer(const char * host, int display) {
    strcpy(this->host, host);
    vncServerDisplayNum = display;
}

void Config::setVncServer(const char* hostDisplay) {
    const char * errorMsg = "Server parameter has to be in format <host>:<display>. Host can be empty for localhost.";
    const char * delimiter = strstr(hostDisplay, ":");
    if (delimiter == NULL) throw Exception(errorMsg);
    
    if (delimiter == hostDisplay) sprintf(host, "localhost");
    else {
        memcpy(host, hostDisplay, (delimiter-hostDisplay)*sizeof(char));
        host[(delimiter-hostDisplay)+1] = 0;
    }
    int display = atoi(delimiter+1);
    
    if (display == 0) throw Exception(errorMsg);
    else this->vncServerDisplayNum = display;
}

void Config::setServerMode(Config::Mode serverMode) {
    this->serverMode = serverMode;
}

void Config::setClientMode(Config::Mode clientMode) {
    this->clientMode = clientMode;
}

void Config::setLogType(Config::LogType logType) {
    this->logType = logType;
}

void Config::setImageDir(const char * dir) {
    if (strlen(dir) >= MAX_STRING_LENGTH) {
        throw Exception("Maximum lengt of imagedir is %d.", MAX_STRING_LENGTH);
    }
    
    memcpy(imageDir, dir, sizeof(char)*strlen(dir) + 1);
}

void Config::setRecordFile(const char * file) {
    if (strlen(file) >= MAX_STRING_LENGTH) {
        throw Exception("Maximum length of recordfile is %d.", MAX_STRING_LENGTH);
    }
    
    memcpy(recordFile, file, sizeof(char)*strlen(file) + 1);
}

void Config::setLogFile(const char * file) {
    if (strlen(file) >= MAX_STRING_LENGTH) {
        throw Exception("Maximum length of logfile is %d.", MAX_STRING_LENGTH);
    }
    
    memcpy(logFile, file, sizeof(char)*strlen(file) + 1);
}

void Config::setProxyDisplay(const char * port) {
    int p = atoi(port);
    if (p == 0) throw Exception("Invalid port for vncproxy.");
    
    this->proxyDisplayNum = p;
}

void Config::setServerMode(const char * serverMode) {
    if (!strcmp(serverMode, "transparent")) {
        this->serverMode = TRANSPARENT;
    } else if (!strcmp(serverMode, "save")) {
        this->serverMode = SAVE;
    } else {
        throw Exception("Unsupported server mode.");
    }
}

void Config::setClientMode(const char * clientMode) {
    if (!strcmp(clientMode, "transparent")) {
        this->clientMode = TRANSPARENT;
    } else if (!strcmp(clientMode, "save")) {
        this->clientMode = SAVE;
    } else if (!strcmp(clientMode, "replay")) {
        this->clientMode = REPLAY;
    } else {
        throw Exception("Unsupported client mode.");
    }
}

void Config::setLogType(const char * logType) {
    if (!strcmp(logType, "file")) {
        this->logType = FILE;
    } else if (!strcmp(logType, "stdout")) {
        this->logType = STDOUT;
    } else if (!strcmp(logType, "all")) {
        this->logType = ALL;
    } else {
        throw Exception("Unsupported log type.");
    }
}

const char * Config::getHost() const {
    return host;
}