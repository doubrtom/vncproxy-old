/* 
 * File:   Config.h
 * Author: tomas
 *
 * Created on 5. únor 2014, 20:07
 */

#ifndef CONFIG_H_8987654546728926
#define	CONFIG_H_8987654546728926

namespace doubrtom {
    
    class Config {
    public:
        
        static const int MAX_STRING_LENGTH = 254;
        
        enum Mode {
            TRANSPARENT, REPLAY, SAVE
        };
        
        enum LogType {
            STDOUT, FILE, ALL
        };
        
        Config();
        void processParameters(int argc, char ** argv);
        void readDisplayNumber(const char * str);
        void createProgramDir();
        void readConfigFile();
        void checkConfig();
        
        int getProxyDisplay() const;
        int getVncServerDisplay() const;
        Mode getServerMode() const;
        Mode getClientMode() const;
        LogType getLogType() const;
        const char * getImageDir() const;
        const char * getRecordFile() const;
        const char * getLogFile() const;
        const char * getHost() const;
        
        void setValue(const char * key, const char * value);
        
        void setProxyDisplay(int port);
        void setProxyDisplay(const char * port);
        void setVncServer(const char * host, int display);
        void setVncServer(const char * hostDisplay);
        void setServerMode(Mode serverMode);
        void setServerMode(const char * serverMode);
        void setClientMode(Mode clientMode);
        void setClientMode(const char * clientMode);
        void setLogType(LogType logType);
        void setLogType(const char * logType);
        void setImageDir(const char * dir);
        void setRecordFile(const char * file);
        void setLogFile(const char * file);
        
        
    protected:
        const char * homeDir;
        char programDir[MAX_STRING_LENGTH];
        char host[MAX_STRING_LENGTH];
        int proxyDisplayNum; // port for listen
        int vncServerDisplayNum; // port with vncserver
        Mode serverMode;
        Mode clientMode;
        LogType logType;
        char imageDir[MAX_STRING_LENGTH];
        char recordFile[MAX_STRING_LENGTH];
        char logFile[MAX_STRING_LENGTH];
    };
    
}


#endif	/* CONFIG_H */

