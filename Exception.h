/* 
 * File:   Exception.h
 * Author: tomas
 *
 * Created on 5. únor 2014, 20:42
 */

#ifndef EXCEPTION_H_5678987678
#define	EXCEPTION_H_5678987678

namespace doubrtom {
 
    class Exception {
    public:
        Exception();
        Exception(const char * msg);
        Exception(const char * msg, int arg);
        Exception(const char * msg, const char * arg);
        const char * getMessage() const;
        
    protected:
        char  msg[255];
    };
    
}

#endif	/* EXCEPTION_H */

