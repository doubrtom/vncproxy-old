#include <cstdlib>
#include <signal.h>
#include <iostream>
#include <cstdio>

#include "Container.h"
#include "Config.h"
#include "Exception.h"
#include "Logger.h"

using namespace std;
using namespace doubrtom;

static void signalHandler(int signum) {
    
}

void printUsage(const char * programName) {
    printf("Vnc proxy server.\n");
    printf("Usage: %s :display [parameters]\n", programName);
    printf("Parameters insert in format: -key value\n");
}


int main(int argc, char** argv) {
    
    if (argc == 1) {
        printUsage(argv[0]);
        return 0;
    }
    
    // register signals
    signal(SIGHUP, signalHandler);
    signal(SIGKILL, signalHandler);
    signal(SIGTERM, signalHandler);
    
    Config & config = Container::getInstance().getConfig();
    
    // process parameters
    try {
        config.createProgramDir();
        config.readConfigFile();
        config.processParameters(argc, argv);
        config.checkConfig();
    } catch (doubrtom::Exception & ex) {
        cerr << "ERROR: " << ex.getMessage() << endl;
        // clear container
        Container::getInstance().clear();
        exit(1);
    }
    
    printf("Display proxy je %d, display vncserver je %d a host je %s.\n", config.getProxyDisplay(), config.getVncServerDisplay(), config.getHost());
    printf("Mode client je %d a server mode je %d\n", (int)config.getClientMode(), (int)config.getServerMode());
    
    // clear container
    Container::getInstance().clear();
    
    return 0;
}

