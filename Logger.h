/* 
 * File:   Logger.h
 * Author: tomas
 *
 * Created on 8. únor 2014, 18:03
 */

#ifndef LOGGER_H_645678918276
#define	LOGGER_H_645678918276

#include <fstream>
#include <string>

namespace doubrtom {
    
    class Logger {
    public:
        enum LogType { ERROR, INFO };
        
        Logger(const char * filePath = 0, bool useStdout = false);
        virtual ~Logger();
        
        virtual void error(const char * str, ...);
        virtual void info(const char * str, ...);
        virtual void write(LogType type); // print out buffer
        
        const std::string getDateTime();
    protected:
        char buffer[255];
        std::ofstream file;
        bool useStdout;
        bool useFile;
    };
    
}

#endif	/* LOGGER_H */

